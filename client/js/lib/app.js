// Libs
import Angular from 'angular';
import angularMeteor from 'angular-meteor';
import 'angular-animate';
import 'angular-meteor-auth';
import 'angular-sanitize';
import 'angular-moment';
import 'angular-ui-router';
import Loader from 'angular-ecmascript/module-loader';
import { Meteor } from 'meteor/meteor';
 
// Modules
import Definer from '../definer';
import RoutesConfig from '../routes';
import MainCtrl from '../controllers/main.controller.ng.js';
import SuccessCtrl from '../controllers/success.controller.ng.js';


// App

App = Angular.module('lettil', [
  angularMeteor,
  'angular-meteor.auth',
  'accounts.ui',
  'ngAnimate',
  'ui.router',
  'ngSanitize',
  'angularMoment'
  

]);

new Loader(App)
  .load(RoutesConfig)
  .load(MainCtrl)
  .load(SuccessCtrl)
 
 
// // Startup
// if (Meteor.isCordova) {
//   angular.element(document).on('deviceready', onReady);
// }
// else {
//   angular.element(document).ready(onReady);
// }
 
// function onReady() {
//   angular.bootstrap(document, ['base']);
// }

