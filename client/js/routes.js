import { Config } from 'angular-ecmascript/module-helpers';
 
export default class RoutesConfig extends Config {
  configure() {
    this.$stateProvider
      .state('header', {
        url: '/header',
        abstract: true,
        templateUrl: 'client/templates/header.html'
      })
      .state('main', {
        url: '/main',
        templateUrl: 'client/templates/main.html',
        controller: 'MainCtrl as main'
      })
      .state('toAddress', {
        url: '/toAddress',
        templateUrl: 'client/templates/toAddress.html',
        controller: 'MainCtrl as main'
      })
      .state('fromAddress', {
        url: '/fromAddress',
        templateUrl: 'client/templates/fromAddress.html',
        controller: 'MainCtrl as main'
      })
      .state('csvUpload', {
        url: '/csvUpload',
        templateUrl: 'client/templates/csvUpload.html',
        controller: 'MainCtrl as main'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'client/templates/about.html',
      })
      .state('policies', {
        url: '/policies',
        templateUrl: 'client/templates/policies.html',
      })
      .state('faq', {
        url: '/faq',
        templateUrl: 'client/templates/faq.html',
      })
      .state('success', {
        url: '/success',
        templateUrl: 'client/templates/success.html',
        controller: 'SuccessCtrl as success'
      });
    this.$urlRouterProvider.otherwise('/main');
  }
}
 
RoutesConfig.$inject = ['$stateProvider', '$urlRouterProvider'];