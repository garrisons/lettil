$(document).ready(function(){
    
    $('#ready #close').on('click', function() {
        $('#ready').closeModal();
    });
});

// tinymce.init({
//   selector: '.wrapper',
//   skin_url: '/packages/teamon_tinymce/skins/lightgray',
//   inline: true,
//   menubar: false,
//   toolbar2: false,
//   plugins: 'paste',
//   invalid_styles: 'color font-size background-color background font-family line-height margin margin-bottom margin-top margin-left margin-right padding-top padding-bottom padding-left padding-right'
// });

WebFontConfig = {
    google: { families: [ 'Tangerine::latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();