import { Controller } from 'angular-ecmascript/module-helpers';
import { Meteor } from 'meteor/meteor';

export default class SuccessCtrl extends Controller {
    constructor() {
        super(...arguments);
        this.orderId = Session.get('orderId');

        this.start();
    }
 
    start($scope, $state) {
        
        if(!this.orderId) {
            this.$state.go('main');
        }

        Meteor.call('getCollectionOfLetters', this.orderId, (err, res) => {
            
            console.log(res);

            if(err) {
                this.error = err;
            }

            if(res) {
                this.res = res;
            }

        });




        

    }

}
SuccessCtrl.$inject = ['$scope','$state'];