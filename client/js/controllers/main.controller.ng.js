import { Controller } from 'angular-ecmascript/module-helpers';
import { Meteor } from 'meteor/meteor';

export default class MainCtrl extends Controller {
    constructor($scope) {
        super(...arguments);

        this.b = 0;

        var that = this;


        this.canOpen = false;

         var handler = StripeCheckout.configure({
            key: 'pk_test_QrwIpvCqoqWe5kQQAAnicDKZ',
            image: '/img/documentation/checkout/marketplace.png',
            locale: 'auto',
            token: function(token) {
            // You can access the token ID with `token.id`.
            // Get the token ID to your server-side code for use

                Meteor.call('paymentSuccess', token, that.totalPrice, (error, response) => {
                    if(error) {
                        Bert.alert( error.reason, 'danger' );
                    } 
                    
                    if(response) {

                        that.onPaymentSuccess(token);
                    }
                });
            }
        });

        $('#pay').on('click', function(e) {
            // Open Checkout with further options:
            $('#ready').closeModal();
            
            handler.open({
                name: 'Lettil',
                description: '1 letter',
                zipCode: true,
                email: that.userEmail,
                amount: that.amount
            });
            e.preventDefault();
        });

        // Close Checkout on page navigation:
        $(window).on('popstate', function() {
            handler.close();
        });
        
        $(document).ready(function() {
            $('select').material_select();
        });

        this.mailMultipleLoad();

        $('#sendLetter').on('click', function() {
            that.sendLetter();
        });

        this.manyAddresses = 'multiple';

        var element = document.querySelector('#actualEditor');
        this.element = element;
        
        this.letterWriter = new MediumEditor(element, {
            toolbar: {
                buttons: ['bold', 'italic', 'h1', 'h2', ],
                diffLeft: 25,
                diffTop: 10,
            }
        }); 
        
        this.forLobFormating();
        
        Session.setDefault('firstTime', 'true');
        this.initModalOpen();
    }

    initModalOpen() {
        //if(session.get('firstTime')) {
            angular.element('#initModal').openModal();
        //}
        
    }

    initModalClose() {
        angular.element('#initModal').closeModal();
        Session.set('firstTime', 'false');
        this.letterWriter.selectElement(this.element.childNodes[0]);
    }

    forLobFormating() {
        var originalHtml = $('#actualEditor').html();
        $('#letterFormattedForPrint').html(originalHtml);


        // finding each word and then seeing if position changes to get new line
        // then populating array to 
            
        $('#letterFormattedForPrint').children().each(function() {
            var originalText = $(this).html();

            // seperating out words
            var stringArray = originalText.split(/(\s+)/);
            var finalText = '';
            stringArray.forEach(function(element) {
                if(element !== '\u0020') {
                    finalText = finalText + '<div class="word">' + element + '</div>';
                } else {
                    finalText = finalText + element;
                }
            }, this);
            finalText = finalText;
            $(this).html(finalText);

            // establishing lines for page breaks
            // Need to store previous offsetTop
            var currentTop = $(this).first('.word').offset().top;
            var finalHtml = [];
            $(this).children('.word').each(function() {
                if($(this).offset().top > currentTop) {
                    var work = '</div><div class="line-view">' + $(this).html();
                    finalHtml.push(work);

                } else {
                    finalHtml.push($(this).html());
                }
            
                currentTop = $(this).offset().top;
            });

            var replaceFinalHtml = '';

            finalHtml.forEach(function(element) {
                replaceFinalHtml = replaceFinalHtml + element + '\u0020'; 
            }, this);

            // removing trailing space with trim
            $(this).html('<div class="line-view">' + replaceFinalHtml.trim() + '</div>');

            // establishing pages

        });

        this.seperateIntoPages();
    }

    seperateIntoPages() {
        var pageNumber = 1;
        this.fullDoc = '<div class="page" id="page1"><div class="wrapper">';
        var endDoc = '</div></div>';
        this.pageId = 1;
        var pageHeight = 0;
        var elem = $('#letterFormattedForPrint');
        var elemChildren = elem.children();
        var page1Height = 670; 
        var newPage = '</div></div><div class="page" id="page' + this.pageId + '"><div class="wrapper">';

        if(elem.height() > page1Height && pageNumber === 1) {

            elemChildren.each(function() {
                if($(this).height() + pageHeight > page1Height) {
                    if(1 < elemChildren.length) {
                        splitChildren($(this));
                    } else {
                        this.fullDoc += newPage + element[0].outerHTML;
                        pageHeight += element.height();
                        this.pageId++;
                    }
                } else {
                    pageHeight += $(this).height();
                    this.fullDoc += $(this)[0].outerHTML;
                    

                }

            });

        } else if(elem.height() > 970 && pageNumber > 1) {  
            elemChildren.each(function() {
                if($(this).height() + pageHeight > page1Height) {
                    if(1 < elemChildren.length) {
                        splitChildren($(this));
                    } else {
                        this.fullDoc += newPage + element[0].outerHTML;
                        pageHeight += element.height();
                        this.pageId++;
                    }
                } else {
                    pageHeight += $(this).height();

                    this.fullDoc += $(this)[0].outerHTML;

                }
            });
        }

        this.fullDoc += endDoc;

        function splitChildren(element) {
                
            var outerTagStart = '<' + element[0].nodeName + '>';
            var outerTagEnd = '</' + element[0].nodeName + '>';
            outerTagStart = outerTagStart.toLowerCase();
            outerTagEnd = outerTagEnd.toLowerCase();
            console.log(outerTagStart);
            
            element.children().each(function() {
                if(1 < $(this).children().length) {
                    splitChildren($(this));
                } else {

                    if($(this).height() + pageHeight > page1Height) {
                        this.fullDoc += newPage + element[0].outerHTML;
                        pageHeight = 0;
                        this.pageId++;
                    } else if($(this).last() && $(this).height() + pageHeight < page1Height) {
                        
                        pageHeight += $(this).height();
                        this.fullDoc += $(this)[0].outerHTML;

                    } else {
                        
                        pageHeight += $(this).height();
                        this.fullDoc += $(this)[0].outerHTML;
                    }

                }
            });


        }
        

        
    }

    openThisModal(id) {
        angular.element('#'+id).openModal();
        if(id === 'ready') {
            this.seperateIntoPages()
        }

    }

    redirectToMain($state) {
        this.$state.go('main');
    }

    checkForPage() {
        
        var that = this;
        a = 0;
        pageHeight = 0;
        letterHtml = '<div class="page"><div class="page-content"><div class="first-page" style="position: absolute;top: 3in; width: 7.5in;">';
        this.numberOfPages = 1;
        

        $('.wrapper').children().each(function() {

            $(this).removeAttr('id');

            var heightNumber = $(this).height();
            var htmlOfElement = $(this).clone().wrap('<div>').parent().html();

            pageHeight = pageHeight + heightNumber;
            if(that.numberOfPages === 1) {
                if(pageHeight > 700) {
                    var id = 'firstElementOfPage'+a;
                    $(this).attr('id', id);
                    pageHeight = 0;
                    letterHtml = letterHtml + '</div></div></div><div class="page"><div class="page-content"><div class="other-pages">' + htmlOfElement;
                    that.numberOfPages++;

                } else {
                    letterHtml = letterHtml + htmlOfElement;
                    $(this).removeAttr('id');
                }
            } else {
                if(pageHeight > 900) {
                    var id = 'firstElementOfPage'+a;
                    pageHeight = 0;
                    letterHtml = letterHtml + '</div></div></div><div class="page"><div class="page-content"><div class="other-pages">' + htmlOfElement;
                    $(this).attr('id', id);
                    this.numberOfPages++;

                } else {
                    letterHtml = letterHtml + htmlOfElement;
                    $(this).removeAttr('id');
                }
            }

        });

        if($('h2.signature').text().length > 0) {
            var sigHeight = $('h2.signature').height();
            var sigHtml = $('h2.signature').clone().wrap('<div>').parent().html();
            pageHeight = sigHeight + pageHeight;
            if(numberOfPages === 1) {
                if(pageHeight > 700) {
                    var id = 'firstElementOfPage'+a;
                    $(this).attr('id', id);
                    pageHeight = 0;
                    letterHtml = letterHtml + '</div></div></div><div class="page"><div class="page-content"><div class="other-pages">' + sigHtml;
                    numberOfPages++;

                } else {
                    letterHtml = letterHtml + sigHtml;
                    $(this).removeAttr('id');
                }
            } else {
                if(pageHeight > 900) {
                    var id = 'firstElementOfPage'+a;
                    pageHeight = 0;
                    letterHtml = letterHtml + '</div></div></div><div class="page"><div class="page-content"><div class="other-pages">' + sigHtml;
                    $(this).attr('id', id);
                    this.numberOfPages++;

                } else {
                    letterHtml = letterHtml + sigHtml;
                    $(this).removeAttr('id');
                }
            }

        }
        

        this.finalHtmlToLob = letterHtml + '</div></div></div>';
        
    }
    
    uploadFile() {
        var that = this;

        var email = that.userEmail; 

        that.amount = 200;

        this.numberOfAddresses = 0;

        if($('input[type=file]#csv-uploader').val() === '') {
            return false;
        }

        this.$scope.mailMultiple = true;

        Meteor.call('newAddressBook', email, (err, res) => {
            if(err) {
                console.log(error);
            }

            if(res) {
                newAddressBookId = res;
                
                $('input[type=file]#csv-uploader').parse({
                    config: {
                        // base config to use for each file
                        delimiter: "",	// auto-detect
                        newline: "",	// auto-detect
                        header: true,
                        dynamicTyping: false,
                        preview: 0,
                        encoding: "",
                        worker: false,
                        comments: false,
                        complete: function(results) {
                            console.log(results);
                        },
                        error: undefined,
                        download: false,
                        skipEmptyLines: true,
                        chunk: undefined,
                        fastMode: undefined,
                        beforeFirstChunk: undefined,
                        withCredentials: undefined,
                        step: function(results, parser) {
                            // validateFields
                            if(!results.data[0].address1) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            }
                            if(!results.data[0].city) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            } 
                            if(!results.data[0].state) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            } 
                            if(!results.data[0].country_code) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            } 
                            if(!results.data[0].to) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            } 
                            if(!results.data[0].zip) {
                                parser.pause();
                                Bert.alert( 'There is an error on row' + i, 'danger', 'growl-top-right' );
                                that.numberOfAddresses = 0;
                                return false;
                            }
                            that.numberOfAddresses++;
                            that.saveAddresses(results, newAddressBookId);
                            
                        }
                    },
                    before: function(file, inputElem)
                    {
                        that.beingUploaded = true;

                    },
                    error: function(err, file, inputElem, reason)
                    {
                        // executed if an error occurs while loading the file,
                        // or if before callback aborted for some reason
                        
                        console.log('There was an error');
                    },
                    complete: function(results, file)
                    {
                        amountToChargePerAddress = that.numberOfAddresses * 95;
                        that.totalPrice = that.amount + amountToChargePerAddress;
                        Bert.alert( 'CSV import was a success.', 'success', 'growl-top-right' );
                        
                    }

                });

            }

        });

    }
    
    mailMultipleLoad($scope) {
        this.$scope.mailMultiple = false;
    }

    removeCsv() {

        var cloned = angular.element('#csv-to-address .file-field').clone().html();
        angular.element('#csv-to-address .file-field').html(cloned);
        
        this.$scope.mailMultiple = false;

    }

    saveAddresses(results, addressesBookId, $scope) {
        
        

        this.addressesBookId = addressesBookId;
        console.log(this.addressesBookId);
        var addressObj = results.data[0];

        Meteor.call('addAddress', addressObj, addressesBookId, (err, res) => {

            if (err) {
                console.log(err);
            }

            if (res) {
                console.log(res);
            }

        });

    }

    validateFields(fieldModel, type, fieldName) {
        if (angular.isUndefined(fieldModel) | fieldModel === '') {
            Bert.alert( 'Looks like the "' + fieldName + '" is missing.', 'danger', 'growl-top-right' );
            this.b = 0;
            console.log('here');
            return false;
        }
        
        if(check(fieldModel, type)) {
            console.log('here2');
            return false;
        }
        
        this.b++

        return true;
    }

    checkFields($scope) {
        var html_content = this.$scope.finalHtmlToLob;
        var string = String;
        console.log('hello');
        
        if(!this.validateFields(html_content, string, 'Letter Content')) {
            return false;
        }

        if(!this.addressesBookId) {
            if(!this.validateFields(this.data.to_name, string, 'To Name')) {
                return false;
            }
            if(!this.validateFields(this.$scope.to_address, string, 'To Address')) {
                return false;
            }
            if(!this.validateFields(this.$scope.to_address_city, string, 'To Address City')) {
                return false;
            }
            if(!this.validateFields(this.$scope.to_address_zip, string, 'To Address Zip')) {
                return false;
            }
            if(!this.validateFields(this.$scope.to_address_state, string, 'To Address State')) {
                return false;
            }               
            
        }
        
        if(!this.validateFields(this.$scope.from_address_zip, string, 'From Address Zip')) {
            return false;
        }
        if(!this.validateFields(this.$scope.from_address_state, string, 'From Address State')) {
            return false;
        }
        if(!this.validateFields(this.$scope.from_address_city, string, 'From Address City')) {
            return false;
        }
        if(!this.validateFields(this.$scope.from_address, string, 'From Address')) {
            return false;
        }

        if(!this.validateFields(this.$scope.userEmail, string, 'Email')) {
            return false;
        }

        if(this.validateFields(this.$scope.from_name, string, 'From Name')) {   
            return true;
        } else { 
            return false;
        }


    }

    sendLetter(checkFields) {
        
        this.checkForPage();

        //Order matters for error message last shows error first
        
        this.b = 0;



        if(this.checkFields()) {
            this.openModal();
        }
        
        
    }

    openModal() {
        $('#ready').openModal({
            dismissable: true
        });
    }


    capturePayment(token) {
        check(token, Object);
        Meteor.call('stripeCharge', token, (err, res) => {
            if(err) {
                console.log(err);
            }

            if(res) {
                console.log(res);
            }
        });

    }

    onPaymentSuccess($scope) {
        var that = this;

        if(this.$scope.mailMultiple) {
            this.callMethod('sendMultipleLetter', {

                addressesBookId: this.addressesBookId,
                to_name: this.to_name,
                to_address_line1: this.$scope.to_address,
                to_address_city: this.$scope.to_address_city,
                to_address_state: this.$scope.to_address_state,
                to_address_zip: this.$scope.to_address_zip,
                to_address_country: 'US',
                email: this.$scope.userEmail,
                html: this.$scope.finalHtmlToLob,

            }, (err, res) => {
                that.start(err, res);
                

            });
        } else {
            this.callMethod('sendSingleLetter', {
                from_name: this.$scope.from_name,
                from_address_line1: this.$scope.from_address,
                from_address_city: this.$scope.from_address_city,
                from_address_state: this.$scope.from_address_state,
                from_address_zip: this.$scope.from_address_zip,
                from_address_country: 'US',
                
                to_name: this.$scope.to_name,
                to_address_line1: this.$scope.to_address,
                to_address_city: this.$scope.to_address_city,
                to_address_state: this.$scope.to_address_state,
                to_address_zip: this.$scope.to_address_zip,
                to_address_country: 'US',
                email: this.userEmail,
                html: this.$scope.finalHtmlToLob,
                
            }, (err, res) => {
                that.start(err, res);
                
            });
        }
    }

    start(err, res, $state) {

        if(err) {
            console.log(err);
        }

        Session.set('orderId', res);

        this.$state.go('success');

    }

    
}

MainCtrl.$inject = ['$scope', '$state', '$stateParams', '$reactive'];