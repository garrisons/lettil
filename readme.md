# Penletters.xyz

Be sure to check the lob wrapper is installed.

**Specs**

This is what we want... We want users to be able to send a letter for $3.

| Meal    | Price |
|---------|----------------|
| Lob Initial Fee  | $0.95 |
| Add on page | $0.10 |
| 2.9% + 0.30 for Stripe | $0.39 |
| Final Cost | $1.44 |

So that means $1.56 in proft. Wishful thinking in the print industry...

Lob charges your payment method at the end of every month. We'll need to get Paypal Payments Pro to have instant access to cash, but not a show stopper

So what features does it need?

* Enter text
* From address
* To address
* Save letter # to email address in DB table
* Signatures 
    * This can just be an input box that uses some fancy google font. Doesn't need docusign.

Nice To Haves:

* Multiple Addresses
* Previous letters
* Address book
* Preview
    * Needs to send the letter to lob and get the response PDF.


I'm wondering if I need to expand the feature set... I'm wondering if the MVP should be a wabam.
The benefits though... That for each new feature it's an additional press release. I think I'm
going with that logic.

Total features should be...

* Enter text
* From address
* To address
* Account
* Signatures
* Multiple To Addresses
* Previous letters
* Address book

-----------------------------------------

So I've managed to get Lob connected to my rough draft of the frontend. These are some items that I need to take care of:

* Figure out multiple entries via lob maybe?
* integrate Stripe
* Add user accounts
* Figure out pricing for mass mail
* save response to DB

###Solution For Page Issue With Lob

Add in .pages and .wrappers to html when overflow is triggered 


----
Everything is done except for the confirmation emails...

And I want to do a redesign.

Curently the features are...

* Send a letter in wysiwyg
* Send to multiple people or one

I want to add in...

* A better design - more like hyperdev
* A better WYSIWYG
* User Accounts for CSV Upload
* Address Verification
* PDF Upload
* Post Cards


Now, what can I do???

* A better design
* A better wysiwyg
* User Accounts for CSV Upload
* Address verification



wysiwyg
<br> for enters instead of new text new elements
if width of x is reached look for last space in string else looks for last character
and seperate into same element


-----------

Hey everyone! I am a millennial and I hate mailing letters. So I wanted to make a web app to allow people to easily mail letters.
Then I realized that people will typically send a lot of letters at once and it would be awesome to include CSV Import and inline variables.
So here we are today launching... I'd love to hear some feedback!

One of the reasons why I chose to start with letters was because when I receive in a letter in the mail (From anyone - even the CEO of PNC Bank who obviously didn't mail me a letter) I read it. Turns out that I'm not the only one who does that.

80% of mail is read. So what that means is that even in the internet age mailing someone a letter is still reliable way to communicate with them. Weather that be mom, dad, or a potential sales lead.