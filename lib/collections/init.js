import { Mongo } from 'meteor/mongo';

letters = new Mongo.Collection('Letters');
letterErrors = new Mongo.Collection('letterErrors');
addressBook = new Mongo.Collection('addressBook');
addresses = new Mongo.Collection('addresses');
lettersResponses = new Mongo.Collection('lettersResponses');

addressBook.schema = new SimpleSchema({
    email: {type: String},
    addressBookId: {type: String},
})

addresses.schema = new SimpleSchema({
    email: {type: String},
    addressBookId: {type: String},
    address: {type: [Object]},
    "address.$.to":{type: String},
    "address.$.street":{type: String},
    "address.$.street2":{type: String},
    "address.$.zip":{type: String},
    "address.$.state":{type: String},
    "address.$.city":{type: String},
    "address.$.country":{type: String},
    res: {type: [Object]}
});