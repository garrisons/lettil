import { Meteor } from 'meteor/meteor';

var Future = Npm.require( 'fibers/future' );

Meteor.startup(() => {
  // code to run on server at startup
  
});
// Ensuring every user has an email address, should be in server-side code
Accounts.validateNewUser((user) => {
  new SimpleSchema({
    _id: { type: String },
    emails: { type: Array },
    'emails.$': { type: Object },
    'emails.$.address': { type: String },
    'emails.$.verified': { type: Boolean },
    createdAt: { type: Date },
    services: { type: Object, blackbox: true }
  }).validate(user);

  // Return true to allow user creation to proceed
  return true;
});
if(Meteor.isServer) {
    process.env.MAIL_URL = 'smtp://lettil:iron1254@smtp.sendgrid.net:587';

  Meteor.methods({

    paymentSuccess: function(stripeToken, amount) {

      var future = new Future();
      var Stripe = StripeAPI(Meteor.settings.private.stripe);
      var response = {

      };


      Stripe.charges.create({
        amount: amount,
        currency: 'usd',
        source: stripeToken.id
      }, function(err, charge) {
  	    if(err) {
          response.err = err;
        }

        if(charge) {
          response.charge = charge;
        }

      });
      console.log(response);
      return response;

    }

  });


}