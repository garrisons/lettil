import { meteor } from 'meteor/meteor';
import '/lib/collections/init';
import 'lob';

var Future = Npm.require( 'fibers/future' ); 

var Lob = require('lob')('test_94f13b1bb9bc186da2fac88938d03a85a5e');

var that = this;

function saveLetterError(res, emailAddress) {
    //var future = new Future();
    res.email = emailAddress;
    lettersResponses.insert({err});
}; 


function saveLetterResponse(res, emailAddress) {
    //var future = new Future();
    res.email = emailAddress;
    lettersResponses.insert({res});
}; 

function emailConfirmation(emailAddress, orderId) {

    
    console.log(emailAddress);
    console.log(orderId);

    // Email.send({
    //     from: "info@lettil.com",
    //     to: emailAddress,
    //     subject: "C",
    //     text: "Here is some text",
    // });

}

Meteor.methods({

    newAddressBook: function(email) {
        
        var countAddressBook = addressBook.find({email: email}).count();
        addressBookId = countAddressBook + 1;  

        return addressBook.insert({email: email, addressBookId: 1});
    },

    addAddress: function (addressObj, addressesBookId) {
        var future = new Future();
        //address.insert({email: email, addressBookId: 1});
        //addressObj.to, addressObj.address1, addressObj.address2, addressObj.city, addressObj.state, addressObj.zip. addressObj.country_code, addressesBookId
        
        addresses.insert({address: addressObj, addressBookId: addressesBookId});

    },

    sendMultipleLetter: function (object) {
        
        var html_start = '<html>';
        var html_head = '<head><title>Lob.com Outstanding Balance Letter Template</title><link href="https://fonts.googleapis.com/css?family=Tangerine:700" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet"><style>body {width: 8.5in;height: 11in;margin: 0;padding: 0; font-family: "Open Sans", sans-serif;}.page-content {width: 7.5in;height: 10.625in;margin: 0.5in;}h1,h2,h3,h4,h5,h6,p{margin:0 0 5px}h1{font-size:30pt}h2{font-size:26pt}h3{font-size:23pt}h4{font-size:21pt}h5{font-size:19pt}h6{font-size:17pt}p{font-size:12pt}strong{font-weight:700}em{font-style:italic}h2.signature{font-family: "Tangerine", cursive;font-size: 40pt;margin-left:10px;margin-top:10px;border-bottom: 1px solid #777; display: inline-block;}</style></head>';
        var html_wrapper = '<body>';
        var html_close = '</body></html>';
        
        var letterContent = html_start + html_head + html_wrapper + object.html + html_close;

        address = addresses.find({addressBookId: object.addressesBookId}).fetch();

        from = {
            address_line1: object.from_address_line1,
            city: object.from_address_city,
            state: object.from_address_state,
            zip: object.from_address_zip,
            country: object.from_address_country
        }

        address.forEach(function(thisObject) {
            var future = new Future();

            var currentId = letters.insert({email: object.email, to: thisObject, from: from, html: letterContent, addressesBookId: object.addressesBookId});


            Lob.letters.create({
                description: object.description,
                to: {
                    name: thisObject.address.to,
                    address_line1: thisObject.address.address1,
                    address_line2: thisObject.address.address2,
                    address_city: thisObject.address.city,
                    address_state: thisObject.address.state,
                    address_zip: thisObject.address.zip,
                    address_country: thisObject.address.country_code
                },
                from: {
                    name: '160 Ross', //object.from_name,
                    address_line1: '160 N Ross St 1125', //object.from_address_line1,
                    address_city: 'Auburn',//object.from_address_city,
                    address_state: 'AL',//object.from_address_state,
                    address_zip: '36832',//object.from_address_zip,
                    address_country: 'US'//object.from_address_country,
                },
                file: letterContent,
                color: false
            }, function (err, res) {

                if(err) {

                    Future.task(function() {
                        lettersResponses.insert({ParentId: currentId, err: err, addressesBookId: object.addressesBookId});
                    });
                    
                    future.return(err);

                } 
                
                if(res) {
                    
                    Future.task(function() {
                        lettersResponses.insert({ParentId: currentId, res: res, addressesBookId: object.addressesBookId});
                    });

                    future.return(res);
                    
                    
                }

            });

            return future.wait();

        }, this);

        this.emailConfirmation(object.email);

        return object.addressesBookId;

    },

    sendSingleLetter: function (object) {

        var future = new Future();
        
        var html_start = '<html>';
        var html_head = '<head><title>Lob.com Outstanding Balance Letter Template</title><link href="https://fonts.googleapis.com/css?family=Tangerine:700" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i" rel="stylesheet"><style>body {width: 8.5in;height: 11in;margin: 0;padding: 0; font-family: "Open Sans", sans-serif;}.page-content {width: 7.5in;height: 10.625in;margin: 0.5in;}h1,h2,h3,h4,h5,h6,p{margin:0 0 5px}h1{font-size:30pt}h2{font-size:26pt}h3{font-size:23pt}h4{font-size:21pt}h5{font-size:19pt}h6{font-size:17pt}p{font-size:12pt}strong{font-weight:700}em{font-style:italic}h2.signature{font-family: "Tangerine", cursive;font-size: 40pt;margin-left:10px;margin-top:10px;border-bottom: 1px solid #777; display: inline-block;}</style></head>';
        var html_wrapper = '<body>';
        var html_close = '</body></html>';
        
        var letterContent = html_start + html_head + html_wrapper + object.html + html_close;
        
        to = {
            name: object.to_name,
            address_line1: object.to_address_line1,
            address_line2: object.to_address_line2,
            address_city: object.to_address_city,
            address_state: object.to_address_state,
            address_zip: object.to_address_zip,
            address_country: object.to_address_country,
        };

        from = {
            name: object.from_name,
            address_line1: object.from_address_line1,
            address_city: object.from_address_city,
            address_state: object.from_address_state,
            address_zip: object.from_address_zip,
            address_country: object.from_address_country,
        };

        var currentId = letters.insert({email: object.email, to: to, from: from, html: letterContent, addressesBookId: null});

        Lob.letters.create({
            description: object.description,
            to: {
                name: 'Garrison Snelling',//object.to_name,
                address_line1: '136 Glen Abbey Way',//object.to_address_line1,
                address_city: 'Alabaster',//object.to_address_city,
                address_state: 'AL',//object.to_address_state,
                address_zip: '35007',//object.to_address_zip,
                address_country: 'US'//object.to_address_country,
            },
            from: {
                name: '160 Ross', //object.from_name,
                address_line1: '160 N Ross St 1125', //object.from_address_line1,
                address_city: 'Auburn',//object.from_address_city,
                address_state: 'AL',//object.from_address_state,
                address_zip: '36832',//object.from_address_zip,
                address_country: 'US'//object.from_address_country,
            },
            file: letterContent,
            color: false
        }, function (letterErr, letterRes) {

            if(letterErr) {

                Future.task(function() {
                    lettersResponses.insert({ParentId: currentId, letterErr: letterErr});

                    Meteor.call('emailProblem', object.email, currentId, (emailErr, emailRes) => {
                        if(emailErr) {
                            lettersResponses.insert({parentId: currentId, letterErr: letterErr, emailSent: false, emailErr: emailErr});
                            console.log(emailErr);
                        }

                        if(emailRes) {
                            lettersResponses.insert({parentId: currentId, letterErr: letterErr, emailSent: true, emailRes: emailRes});
                            console.log(emailRes);
                        }
                    });

                });

                future.return(currentId);

            }
            
            if(letterRes) {
        
                Future.task(function() {
                    
                    Meteor.call('emailConfirmation', object.email, currentId, (emailErr, emailRes) => {
                    if(emailErr) {
                        lettersResponses.insert({parentId: currentId, letterRes: letterRes, emailSent: false, emailErr: emailErr});
                        console.log(emailErr);
                    }

                    if(emailRes) {
                        lettersResponses.insert({parentId: currentId, letterRes: letterRes, emailSent: true, emailRes: emailRes});
                        console.log(emailRes);
                    }
                });


                });

                future.return(currentId);
            }

        });

        return future.wait();
        
    },
    
    emailConfirmation: function (emailAddress, orderId) {
        console.log(emailAddress);
        console.log(orderId);
    },


    getCollectionOfLetters: function (orderId) {

        if(letters.find({addressesBookId: orderId}).fetch().length > 0) {
            
            var sentToLob = letters.find({addressesBookId: orderId}).fetch();
            var responseFromLob = lettersResponses.find({addressesBookId: orderId}).fetch();
            multipleAddresses = {
                sentToLob: sentToLob, 
                responseFromLob: responseFromLob
            };
            
            console.log('MultipleAddresses:' + multipleAddresses);
            var finalResponse = multipleAddresses;

        } else {

            var sentToLob = letters.find({_id: orderId}).fetch()
            var responseFromLob  = lettersResponses.find({parentId: orderId}).fetch()

            var singleAddress = {
                sentToLob: sentToLob[0],
                responseFromLob: responseFromLob[0]
            };

            var finalResponse = singleAddress;

        }

        return finalResponse;


    }

});